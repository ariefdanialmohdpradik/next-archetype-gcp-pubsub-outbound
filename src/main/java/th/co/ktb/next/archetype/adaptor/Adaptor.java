package th.co.ktb.next.archetype.adaptor;

import th.co.ktb.next.archetype.model.request.SampleApiRequest;
import th.co.ktb.next.archetype.model.response.SampleApiResponse;

/*
* This is the interface to perform outbound API call.
* This interface contains the methods signature of the function to make outbound API call.
* */
public interface Adaptor  {
    SampleApiResponse sendToTopic(SampleApiRequest sampleApiRequest);
    String sendToTopic1(SampleApiRequest sampleApiRequest);
}
