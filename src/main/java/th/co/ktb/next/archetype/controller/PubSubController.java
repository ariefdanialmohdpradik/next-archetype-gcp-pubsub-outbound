package th.co.ktb.next.archetype.controller;

import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import th.co.ktb.next.archetype.model.request.SampleApiRequest;
import th.co.ktb.next.archetype.model.response.SampleApiResponse;
import th.co.ktb.next.archetype.service.PostMessageAService;

/*
 * This is the controller layer of the Micro-service.
 * Description:
 * 1. The controller layer will handle those incoming APIs and pass to its respective service.
 * 2. Each controller will be categorized based on a particular API group.
 */

@Log4j2
@RestController
@RequestMapping("/api/v1")
public class PubSubController {

    private PostMessageAService postMessageAService;

    public PubSubController(PostMessageAService postMessageAService) {
        this.postMessageAService = postMessageAService;
    }

    @PostMapping("/send-message")
    public SampleApiResponse publishMessage(@RequestBody SampleApiRequest sampleApiRequest) {
        return postMessageAService.execute(sampleApiRequest);
    }
}
