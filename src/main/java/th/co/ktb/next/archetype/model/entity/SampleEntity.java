package th.co.ktb.next.archetype.model.entity;

import lombok.*;

//import javax.persistence.*;
/*
* This is the Entity class that used with Spring JPA to access database.
*/
@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class SampleEntity {

    private String topicName;
    private String message;
    private String message1;
}
