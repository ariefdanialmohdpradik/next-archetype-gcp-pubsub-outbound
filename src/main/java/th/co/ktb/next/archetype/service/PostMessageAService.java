package th.co.ktb.next.archetype.service;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import th.co.ktb.next.archetype.adaptor.Adaptor;
import th.co.ktb.next.archetype.model.request.SampleApiRequest;
import th.co.ktb.next.archetype.model.response.SampleApiResponse;
import th.co.ktb.next.telemetry.interceptor.PubSubOutboundInterceptor;

/**
 * This POST service will write messages to MySampleTopic
 */

@Log4j2
@Service
public class PostMessageAService {

    private String TOPIC = "MySampleTopic";

    private Adaptor adaptor;

    public PostMessageAService(Adaptor adaptor) {
        this.adaptor = adaptor;
    }

    public SampleApiResponse execute(SampleApiRequest sampleApiRequest) {

        adaptor.sendToTopic(sampleApiRequest);

        // Todo: sample telemetry with interceptor
        PubSubOutboundInterceptor pubSubOutboundInterceptor = new PubSubOutboundInterceptor();
        pubSubOutboundInterceptor.intercept(TOPIC, sampleApiRequest);

        SampleApiResponse sampleApiResponse = new SampleApiResponse();
        sampleApiResponse.setTopicName(sampleApiRequest.getTopicName());
        sampleApiResponse.setMessage(sampleApiRequest.getMessage());
        sampleApiResponse.setMessage1(sampleApiRequest.getMessage1());

        return sampleApiResponse;
    }
}
