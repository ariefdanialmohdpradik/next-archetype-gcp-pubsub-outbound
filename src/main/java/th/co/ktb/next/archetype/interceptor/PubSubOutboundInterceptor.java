package th.co.ktb.next.archetype.interceptor;

import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;
import th.co.ktb.next.archetype.model.request.SampleApiRequest;

@Log4j2
@Component
public class PubSubOutboundInterceptor {

    public void intercept(String topic, SampleApiRequest message) {
        log.debug("{} {}", topic, message);
    }

    public void intercept(String topic, String subscription, SampleApiRequest message) {
        log.debug("{} {} {}", topic, subscription, message);
    }
}
