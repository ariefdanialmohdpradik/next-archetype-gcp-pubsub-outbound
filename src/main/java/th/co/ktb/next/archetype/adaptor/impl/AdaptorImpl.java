package th.co.ktb.next.archetype.adaptor.impl;

import org.springframework.cloud.gcp.pubsub.core.PubSubTemplate;
import org.springframework.stereotype.Component;
import th.co.ktb.next.archetype.adaptor.Adaptor;
import th.co.ktb.next.archetype.model.request.SampleApiRequest;
import th.co.ktb.next.archetype.model.response.SampleApiResponse;

/*
* This is the implementation class that implement Adaptor Interface.
* This layer contains any logic required to make API calls to other Micro-services and backend API.
* * */

@Component
public class AdaptorImpl implements Adaptor {

    private PubSubTemplate pubSubTemplate;

    public AdaptorImpl(PubSubTemplate pubSubTemplate) {
        this.pubSubTemplate = pubSubTemplate;
    }

    @Override
    public SampleApiResponse sendToTopic(SampleApiRequest sampleApiRequest) {
        pubSubTemplate.publish(sampleApiRequest.getTopicName(), sampleApiRequest);
        return new SampleApiResponse();
    }

    @Override
    public String sendToTopic1(SampleApiRequest sampleApiRequest) {
        pubSubTemplate.publish(sampleApiRequest.getTopicName(), sampleApiRequest);
        return "OK";
    }
}
